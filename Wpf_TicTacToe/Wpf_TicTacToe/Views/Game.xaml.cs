﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf_TicTacToe.ViewModels;

namespace Wpf_TicTacToe.Views
{
    /// <summary>
    /// Logique d'interaction pour Game.xaml
    /// </summary>
    public partial class Game : Page
    {

        GameViewModel _gameViewModel = new GameViewModel();

        public Game()
        {
            InitializeComponent();
            DataContext = _gameViewModel;
        }

        private void ButtonQuit_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri(@"Views\Menu.xaml", UriKind.Relative));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _gameViewModel.CaseClick((Button)sender);
        }
    }
}