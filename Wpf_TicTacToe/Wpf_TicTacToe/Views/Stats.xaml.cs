﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf_TicTacToe.Models;
using Wpf_TicTacToe.ViewModels;

namespace Wpf_TicTacToe.Views
{
    /// <summary>
    /// Logique d'interaction pour Stats.xaml
    /// </summary>
    public partial class Stats : Page
    {
        StatsViewModel _statsViewModel = new StatsViewModel();

        public Stats()
        {
            InitializeComponent();
            DataContext = _statsViewModel;
        }

        private void ButtonHome_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri(@"Views\Menu.xaml", UriKind.Relative));
        }
    }
}
